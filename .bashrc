# ipsec start
# ipsec up <name>
# ipsec status <name>
cd() {
    builtin cd $1;
    ls
}

alias d="dolphin"
alias e="dolphin"
alias explorer="dolphin"
alias upgrade="sudo apt update; sudo apt upgrade -y"
alias locksleep="i3lock-fancy & /usr/bin/sleep 7 && systemctl suspend"

alias weather="curl 'https://wttr.in/M%C3%A4rkisch%20Buchholz?0&lang=de' 2>/dev/null"
alias weatherExtended="curl 'https://wttr.in/k%C3%B6nigs%20wusterhausen?lang=de'"

alias dotfiles='git --git-dir=$HOME/.dotfiles --work-tree=$HOME'

alias restartEmacs="pkill emacs; /usr/local/bin/emacs --daemon &"

alias emacs="emacsclient -c -a /usr/local/bin/emacs"
# Preferred editor for local and remote sessions
#if [[ -n $SSH_CONNECTION ]]; then
export EDITOR="emacsclient -c -a /usr/local/bin/emacs"
#else
  #export EDITOR='mvim'
#fi

t() {
    tl $@
    tl
}

# tgz
unTgz() {
    filename=${1::-4}
    mkdir $filename &&
    cp $filename.tgz $filename
    cd $filename >/dev/null 2>&1
    gzip -dc $filename.tgz | tar xf - >/dev/null 2>&1
    gzip -dc $filename.tar.gz >/dev/null 2>&1 | tar xf - >/dev/null 2>&1
    rm $filename.tgz
    cd ..
}

makeTgz() {
    filename=${1::-1}
    tar -cpzf $filename.tgz $filename
}

configApt() {
    sudo $EDITOR /etc/apt/apt.conf.d/proxy.conf
}

bashrc() {
    $EDITOR ~/.bashrc
    . ~/.bashrc
}

bashlocal() {
    $EDITOR ~/.bashlocal.sh
    . ~/.bashlocal.sh
}


killPort() {
  pid=$(netstat -tulpn | grep $1 | tr -s ' ' | rev | cut -f 2 -d ' ' | cut -f 2 -d '/' | rev)
  # $pid ist nicht leer und es läuft ein Prozess auf der $pid => kill $pid, sonst Fehlermeldung
  [[ ! -z "$pid" ]] && ps -p $pid 1>/dev/null && kill $pid && echo "$pid killed" || echo "Es läuft kein Prozess auf dem Port $1"
}

dockerContainerRmByImage() {
  docker ps -a | awk '{ print $1,$2 }' | grep $1 | awk '{print $1 }' | xargs -I {} docker rm {}
}

cRun() {
  cc $1.c -o $1 && ./$1
}

##################
#### OHMYBASH ####
##################

# Path to your oh-my-bash installation.
export OSH=/home/bzenz/.oh-my-bash
OSH_THEME="gallifrey"
#OSH_THEME="purity"

COMPLETION_WAITING_DOTS="true"
# DISABLE_UNTRACKED_FILES_DIRTY="true"

completions=(
  git
  composer
  ssh
)

aliases=(
  general
)

plugins=(
  git
  bashmarks
  npm
)

source $OSH/oh-my-bash.sh
source ~/.local/share/blesh/ble.sh

export LANG=de_DE.UTF-8




# git-Aliase
alias gp="git pull" # aka "git pull"
alias gb="git checkout master && git pull && git checkout -b" # aka "git branch"
alias gc="git clone" # aka "git clone"
alias gs="git status -s ." # aka "git status"
alias gbd="git branch -d" # aka "git branch delete"
alias gdiff="git diff --stat --cached origin/master" # aka "git differences"
alias gab="git branch | grep '*' | cut -f2 -d ' '" # aka "git get active branch"
alias gpush='git push --set-upstream origin $(gab)'
#alias gauthors='for author in "$(git log | grep Author | cut -f 2- -d \' \' | cut -f 1 -d \'<\' | sort | uniq)"; do echo $author; done'

gthrowaway() {
	# setzt den lokalen Stand eines git-Projekts hart auf den Server-Stand zurück und verwirft die lokalen Änderungen
	git fetch origin
    git reset --hard origin/master
}

gdalb() {
	# ake git delete all local branches
	git for-each-ref --format '%(refname:short)' refs/heads | grep -v master | xargs git branch -D
}

alias gm="git checkout master && git pull" # aka "git master"

ggm() {
	cdm && gm # go to mono master
}

alias gbi="git branch -vv" # aka "git branch info"
alias gremove_branches="git for-each-ref --format '%(refname:short)' refs/heads | grep -v master | xargs git branch -D"



# Prüft, ob eine TCP-Verbindung auf dem Server ${1} mit dem Port {2} offen ist und
# erreicht werden kann
tcp_check() {
	nc ${1} ${2} < /dev/null
}



alias please='sudo'

mydu() {
    du -h --max-depth=1
    # als sortierte Ausgabe:
    # du -h --max-depth=1 | sort -h -r
    # -h bei sort bedeute "human readable"
}


# bewirkt, dass die Commands der aktuellen Session nicht in der History landen
suppressHistory() {
  set +o history
}

# Löst die IP-Adresse zum übergebenen Host auf
gethost() {
  getent hosts $1
}

getIP() {
  ip -o -f inet addr show | awk '/scope global/ {print $4}'
}

# include .bashlocal.sh if it exists
if [ -f "$HOME/.bashlocal.sh" ]; then
  . "$HOME/.bashlocal.sh"
fi


###### STARTSCRIPTS ########
if command -v theme.sh > /dev/null; then
  theme.sh gruvbox-dark-pale
fi

eval "$(register-python-argcomplete3 pmbootstrap)"

weather

