# i3 config file (v4)
# Please see http://i3wm.org/docs/userguide.html for a complete reference!
#

set_from_resource $darkred     color1  #000000
set_from_resource $red         color9  #000000
set_from_resource $darkgreen   color2  #000000
set_from_resource $green       color10 #000000
set_from_resource $darkyellow  color3  #000000
set_from_resource $yellow      color11 #000000
set_from_resource $darkblue    color4  #000000
set_from_resource $blue        color12 #000000
set_from_resource $darkmagenta color5  #000000
set_from_resource $magenta     color13 #000000
set_from_resource $darkcyan    color6  #000000
set_from_resource $cyan        color14 #000000
set_from_resource $darkwhite   color7  #000000
set_from_resource $white       color15 #000000

# Use custom colors for black
 set $black       #282828
 set $darkblack   #1d2021
 set $transparent #00000000

set $mod Mod4
set $alt Mod1
set $terminator alacritty

set $leftbtn h
set $downbtn j
set $upbtn k
set $rightbtn l 

# Window color settings
# class                 border  backgr. text    indicator
client.focused          #4C566A #81A1C1 #2E3440 #EBCB8B   #5E81AC
client.focused_inactive #2E3440 #4C566A #ECEFF4 #8FBCBB   #434C5E
client.unfocused        #2E3440 #2E3440 #4C566A #8FBCBB   #3B4252
client.urgent           #BF616A #BF616A #2E3440 #BF616A   #BF616A
client.placeholder      #88C0D0 #2E3440 #B48EAD #2E3440   #2E3440
client.background       #D8DEE9


# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 12

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).

font pango:DejaVu Sans Mono 12
#font pango:Meslo LG L Regular Nerd Font Complete 15

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminator by pressing Mod key + x or ENTER
bindsym $mod+Return exec $terminator
#bindsym $mod+x exec $terminator


# Enable Print Screen
#bindsym --release $mod+Print exec gnome-screenshot -i
bindsym --release $mod+Print exec shutter -s

# kill focused window
bindsym $mod+q kill

# change focus
bindsym $mod+$leftbtn focus left
bindsym $mod+$downbtn focus down
bindsym $mod+$upbtn focus up
bindsym $mod+$rightbtn focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+$leftbtn move left
bindsym $mod+Shift+$downbtn move down
bindsym $mod+Shift+$upbtn move up
bindsym $mod+Shift+$rightbtn move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# move focused workspace between monitors
bindsym $mod+Ctrl+$rightbtn move workspace to output right
bindsym $mod+Ctrl+$leftbtn move workspace to output left
bindsym $mod+Ctrl+Right move workspace to output right
bindsym $mod+Ctrl+Left move workspace to output left

# split in horizontal orientation
bindsym $mod+$alt+h split h

# split in vertical orientation
bindsym $mod+$alt+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
#bindsym $mod+w layout tabbed
bindsym $mod+t layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent
# focus the child container
bindsym $mod+z focus child

# Workspace Variables
set $ws1 "1:"
set $ws2 "2:"
set $ws3 "3:"
set $ws4 "4: +"
set $ws5 "5:?"
set $ws6 "6:"
set $ws7 "7:"
set $ws8 "8:"
set $ws9 "9:"
set $ws10 "10:0"
# set $ws1  1
# set $ws2  2
# set $ws3  3
# set $ws4  4
# set $ws5  5
# set $ws6  6
# set $ws7  7
# set $ws8  8
# set $ws9  9
# set $ws10 10


# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym $leftbtn resize shrink width 10 px or 10 ppt
        bindsym $downbtn resize grow height 10 px or 10 ppt
        bindsym $upbtn resize shrink height 10 px or 10 ppt
        bindsym $rightbtn resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Up resize grow height 10 px or 10 ppt
        bindsym Down resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

# Pulse Audio controls
# run pactl list sinks
#bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5% #increase sound volume
#bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5% #decrease sound volume#
#bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 1 toggle # mute sound

# Amixer

bindsym XF86AudioRaiseVolume exec --no-startup-id amixer -D pulse sset Master 5%+ #increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id amixer -D pulse sset Master 5%- #decrease sound volume#
bindsym XF86AudioMute exec --no-startup-id amixer -q set Master toggle # mute sound

# Sreen brightness controls
# enable passwordless sudo for ybacklight. echo "koromicha ALL=NOPASSWD: /usr/bin/ybacklight" > /etc/sudoers.d/ybacklight
bindsym XF86MonBrightnessUp exec sudo ybacklight -inc 5 # increase screen brightness
bindsym XF86MonBrightnessDown exec sudo ybacklight -dec 5 # decrease screen brightness

# statusbar
# bar {
#
# #-m cpu battery date time pulseaudio \
#     status_command /opt/bumblebee-status/bumblebee-status \
#     -m cpu battery date time \
#     -p time.format="%H:%M CW %V" date.format="%a, %b %d %Y" \
#     -t gruvbox-powerline
#
#     strip_workspace_numbers yes
#     #status_command i3blocks
#     position top
#     font pango:Hack, FontAwesome 14
#
#     colors {
#     #    separator #081419
#     #    background #253941
#         #statusline #839496
#     #    focused_workspace #fdf6e3 #6c71c4 #fdf6e3
#     #    active_workspace #fdf6e3 #6c71c4 #fdf6e3
#     #    inactive_workspace #002b36 #586e75 #002b36
#     #    urgent_workspace #d33682 #d33682 #fdf6e3
#
#     #    statusline         $white
#     #    separator          $transparent
#   }
# }
exec_always --no-startup-id $HOME/.config/polybar/launch.sh


set $m1 #808080 
set $m2 #FFF0E0

# Startup programs
exec --no-startup-id dunst
exec --no-startup-id cups-browsed
exec --no-startup-id clipit &; 
exec --no-startup-id keeweb 
exec --no-startup-id telegram-desktop
exec --no-startup-id firefox
exec --no-startup-id thunderbird
exec --no-startup-id $TERMINATOR
exec --no-startup-id $TERMINATOR -e ranger
exec --no-startup-id $TERMINATOR -e musikcube
#exec --no-startup-id compton
exec_always --no-startup-id nm-applet
exec_always feh --bg-fill ~/Bilder/Wallpaper/wallpaper.png
exec_always pkill compton; compton --vsync opengl-swc --backend glx &
exec_always pkill polybar
exec_always /usr/local/bin/emacs --daemon &
#exec_always compton --config ~/.config/compton.conf;


# Bind App to workspace
# Check class by using xprop command
assign [class="firefox"] $ws2
assign [class="Code"] $ws3
assign [class="Emacs"] $ws3
assign [class="Thunderbird"] $ws6
assign [class="Teams"] $ws6
assign [class="VirtualBox"] $ws7
assign [class="Virt-manager"] $ws7
assign [class="KeeWeb"] $ws8
assign [class="TelegramDesktop"] $ws9


# Assign to certain workspace
#assign [window_role="browser"] $ws2

set $mode_launcher Launcher
bindsym $mod+o mode "$mode_launcher"

mode "$mode_launcher" {
    bindsym f exec firefox, mode "default"
    bindsym e exec "emacsclient -c -a 'emacs'", mode "default"
    bindsym w exec '/usr/lib/virtualbox/VirtualBoxVM --comment "Win10-deg" --startvm "{0b343eb3-9626-4e54-8aad-a9f4bc802e11}"', mode "default"
    bindsym d exec dolphin, mode "default"

    bindsym Escape mode "default"
    bindsym Return mode "default"
}

# Shutdown, Reboot, Lock Screen, and Logout 

set $power_mode "power"
bindsym $mod+Shift+q      mode $power_mode
mode $power_mode {
    bindsym p         exec systemctl poweroff
    bindsym r         exec systemctl reboot 
    bindsym l         exec i3lock-fancy, mode "default"
    #bindsym l         exec i3lock -i ~/Bilder/Wallpaper/lock.png, mode "default"
    bindsym q         exec --no-startup-id i3-msg exit, mode "default"
    bindsym h         exec systemctl hibernate, mode "default"
    bindsym s         exec systemctl suspend, mode "default"
    bindsym Return    mode "default"
    bindsym Escape    mode "default"
}

# Floating windows
for_window [window_role="task_dialog|bubble|page-info|Preferences|pop-up"] floating enable
for_window [window_role="Open Files"] floating enable sticky
for_window [window_role="File Operation Progress"] floating enable sticky
for_window [class="qBittorrent" window_role="pop-up"] floating enable
for_window [window_type="dialog"] floating enable
for_window [window_type="menu"] floating enable

# Sticky window
for_window [instance="file_progress"]  sticky enable
for_window [class="info|Mate-color-select|gcolor2|timesup|QtPass|GtkFileChooserDialog"] sticky enable

# Focus window settings
no_focus [window_role="pop-up"]
focus_on_window_activation focus


# gaps
gaps inner 10
gaps outer 25
#gaps inner all set 20
#gaps outer current plus 5
#gaps horizontal current plus 40
#gaps outer current toggle 60

for_window [class=".*"] border pixel 2

# Custom bindsyms
bindsym $mod+p exec i3-msg exit
#bindsym $mod+p exec ~/.config/i3/bin/logout
#bindsym $mod+l exec i3lock-fancy
#bindsym $mod+l exec i3lock -i ~/Bilder/Wallpaper/lock.png
bindsym $mod+r mode "resize"
bindsym $mod+Shift+f exec ~/.config/i3/bin/rofifinder

# program shortcuts
#bindsym $mod+d exec --no-startup-id rofi -show drun
bindsym $mod+Shift+s exec spectacle --region
bindsym $mod+x exec --no-startup-id dmenu_run
bindsym $mod+d exec --no-startup-id i3-dmenu-desktop
