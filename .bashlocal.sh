
EDITOR="emacsclient -c -a /usr/local/bin/emacs"

alias squirrel="/opt/squirrel-sql-4.2.0/squirrel-sql.sh"
alias win10='/usr/lib/virtualbox/VirtualBoxVM --comment "Win10-deg" --startvm "{0b343eb3-9626-4e54-8aad-a9f4bc802e11}"'

enableBlur() {
    # Enable blur in Yakuake
    if [[ $(ps --no-header -p $PPID -o comm) =~ yakuake ]]; then
        #! /bin/bash
        while true; do
            sleep 0.25
            a=$(xdotool getactivewindow 2>/dev/null)
	    if [ -n "$a" ]; then 
	        xprop -id "$a" -f _KDE_NET_WM_BLUR_BEHIND_REGION 32c -set _KDE_NET_WM_BLUR_BEHIND_REGION 0 2>/dev/null
                set -u
	    fi
        done
    fi
}

tt() {
    if [ "$1" == 's' ]; then
	# an Aufgabe arbeiten (aka start/stop)
	echo create $2
	openTasks=$(tl | grep -e '[0-9]\.' | grep '☐' | cut -f 1 -d '.' | tr -d ' ')
	for task in $openTasks 
	do
		echo $task
	done
    elif [ "$1" == '' ]; then
	tl	
    elif [ "$1" == 'n' ]; then
	taskcount=$(tl | grep -e '[0-9]\.' | wc -l)
	if [ $taskcount ==  ]; then
	 #   id = $()
	 echo hey
	elif [ "${2#PIA-}" != "$2" ]; then
	  # PIA Ticket anlegen
	  tl t --board $2 $2-Testfälle
	  tl t --board $2 $2-Umsetzung
	  tl t --board $2 $2-Devtest
	fi
    fi
}

ueberstunden() {
	localc work/Überstundenliste.xlsx
}
standup() {
     $EDITOR ~/work/Holo/Standup.org
}

webclientProbleme() {
     $EDITOR ~/work/inet/WebClientProbleme.md
}

uni() {
  cd ~/studium/5 1>/dev/null
  $EDITOR 
  cd - 1>/dev/null
}

brightness() {
  if [ $1 == 0 ]; then
    echo Brightness must be a numer and cannot be 0
    exit 1
  fi
  xrandr --output eDP-1 --brightness $1
}

brightnessHDMI1() {
  if [ $1 == 0 ]; then
    echo Brightness must be a numer and cannot be 0
    exit 1
  fi
  xrandr --output HDMI-1 --brightness $1
}

##################
##### STEFAN #####
##################

export BASH_SCRIPT_DIR=~/.scripts
export IDEA_DIR=~/IdeaProjects
export JAVA_PROJECT_DIR=~/IdeaProjects
export MAVEN_BASH_DIR=$BASH_SCRIPT_DIR/maven

alias ds='sudo du -h --max-depth=0' # aka disk space (eines bestimmten Verzeichnisses)

# keepass-Aliases
alias kinet='keepassx ~/.secrets/inet.kdbx'
alias kp='keeweb &'

llg () {
	# aka ll und grep
	ll | grep -i ${1}
}

alias cde="cd $IDEA_DIR"


noproxy() {
        export http_proxy=
        export https_proxy=
        export HTTP_PROXY=
        export HTTPS_PROXY=
}

 

burpproxy() {
        export http_proxy=127.0.0.1:8080
        export https_proxy=127.0.0.1:8080
        export HTTP_PROXY=127.0.0.1:8080
        export HTTPS_PROXY=127.0.0.1:8080
}

 

yesproxy() {
        export http_proxy=http://proxy.data-experts.de:8080
        export https_proxy=http://proxy.data-experts.de:8080
        export HTTP_PROXY=http://proxy.data-experts.de:8080
        export HTTPS_PROXY=http://proxy.data-experts.de:8080
        export NO_PROXY='localhost, 127.0.0.0/8, ::1, *.nlberlin.deg.local, *.data-experts.de, *.data-experts.net, *.nb.deg.local'
}


yesproxyglobal() {
    proxyadress="http://proxy.data-experts.de:8080"
    $BASH_SCRIPT_DIR/assignProxy.sh
    PROXY_ENV="http_proxy https_proxy ftp_proxy all_proxy HTTP_PROXY HTTPS_PROXY FTP_PROXY ALL_PROXY"
    for envar in $PROXY_ENV
    do
        export $envar=$proxyadress
    done 
    for envar in "no_proxy NO_PROXY"
    do
        export $envar="*.local,*.data-experts.de,*.data-experts.net,localhost"
    done
    yesproxy
    git config --global http.proxy $proxyadress
    git config --global https.proxy $proxyadress
}

noproxyglobal() {
    $BASH_SCRIPT_DIR/clearProxy.sh
    PROXY_ENV="http_proxy https_proxy ftp_proxy all_proxy HTTP_PROXY HTTPS_PROXY FTP_PROXY ALL_PROXY"
    for envar in $PROXY_ENV
    do
        unset $envar
    done
    for envar in "no_proxy NO_PROXY"
    do
        unset $envar
    done
    noproxy
    git config --global http.proxy ""
    git config --global https.proxy ""
}

# Maven-Aliase
mcr() {
	# aka maven clear repo
	rm -rf ~/.m2/repository/de/data_experts/*
}

alias mon="export mvn_online=true" # aka "set maven online"
alias moff="export mvn_online=false" # aka "set maven offline"

mcpl() {
    # aka maven clean package locale
    sh ${MAVEN_BASH_DIR}/clean_and_build_local.sh $@
}

mpl() {
    # aka maven package locale
    sh ${MAVEN_BASH_DIR}/build_local.sh $@
}

mwc() {
	# aka make webclient (früher mbb)
	/home/bichi/stefan/bash/development/copy_webclient_classes.sh ${1}
}


mcpla() {
   # aka mcpl all
   for x in {bb,mv,nw,sh,sh-dgl,sn,st}; do (mcpl $x);done
}

prepdocka() {
	for x in {bb,mv,nw,sh,sh,sn,st}; do (prepdock $x);done
}

alias mci="sh ${MAVEN_BASH_DIR}/mvn_clean_install.sh" # aka maven clean install
alias mcp="sh ${MAVEN_BASH_DIR}/mvn_clean_package.sh" # aka maven clean package

mcd() {
    # aka maven copy sources
    sh ${MAVEN_BASH_DIR}/mvn_dependencies_copy_sources.sh
}
mcs() {
    # aka maven copy sources
    sh ${MAVEN_BASH_DIR}/mvn_dependencies_copy_sources.sh
}

alias mdt="sh ${MAVEN_BASH_DIR}/mvn_dependencies_tree.sh" # aka maven dependency tree
alias mfb="sh ${MAVEN_BASH_DIR}/mvn_full_build.sh" # aka maven full build

mliqui() {
	sh ${MAVEN_BASH_DIR}/mvn_install_liquibase.sh ${1}-pod
}

mliquia() {
	# aka mliqui all
	for x in {bb,mv,nw,sh,sh-dgl,sn,st}; do (mliqui $x);done
}

fdeploy() {
	mv /home/bzenz/IdeaProjects/webclient-mono/webClient-${1}-res/src/dev/home/documentDefinition/* /home/bzenz/IdeaProjects/webclient-mono/webClient-${1}-res/src/dev/home/deployment/documentDefinition -v
}

# Programme starten
alias cjm="/usr/bin/firefox http://jenkins.data-experts.de/jenkins/view/inet-buildboard &>/dev/null &" # aka Firefox mit Jenkins-Master
alias cjt="/usr/bin/firefox http://080-lc7-jenkins-test.nlberlin.deg.local/jenkins/script &>/dev/null &"  # aka Firefox mit Jenkins-Test

# aka "cd Webclient-Home"
cdh() {
         cd ${IDEA_DIR}/webclient-mono/webClient-${1}-res${2}/src/main/assembly/home
}
cdm () {
	# aka cd mono
	cd ${IDEA_DIR}/webclient-mono
}
cdw() {
	# aka "cd Webclient working" (analog zu Test-VMs)
	cd ${IDEA_DIR}/webclient-mono/webClient-${1}-res${2}/src/dev/home
}

# ---------------------------
# Aliase für ssh
# ---------------------------
# Jenkins
alias jmaster='ssh bzenz@080-lc7-jenkins-master.nlberlin.deg.local' # aka jenkins master
alias jtest='ssh bzenz@080-lc7-jenkins-test.nlberlin.deg.local' # aka jenkins test
alias ijs='ssh inet@010-lc7-build-standard.nlberlin.deg.local' # aka inet jenkins standard
alias iji='ssh inet@010-lc7-build-integrationstest.nlberlin.deg.local' # aka inet jenkins integrationstest
alias ijt='ssh inet@010-lc7-build-webtarget.nlberlin.deg.local' # aka inet jenkins web_t_arget
alias ijo='ssh inet@010-lc7-build-other.nlberlin.deg.local' # aka inet jenkins other
alias ijno='ssh inet@010-lc7-build-selenium.nlberlin.deg.local' # aka inet jenkins sele_n_ium
alias ijn='ssh inet@010-lu20-build-selenium.nlberlin.deg.local' # aka inet jenkins sele_n_ium


# inet-vms...
alias ibb='ssh inet@inet-bb-test.data-experts.net'
alias imv='ssh inet@inet-mv-test.data-experts.net'
alias inw='ssh inet@inet-nw-test.data-experts.net'
alias ish='ssh inet@inet-sh-test.data-experts.net'
alias isn='ssh inet@inet-sn-test.data-experts.net'
alias ist='ssh inet@inet-st-test.data-experts.net'

alias ibbqs='ssh inet@inet-bb-qs-agreler.data-experts.net'
alias imvqs='ssh inet@inet-mv-qs-agreler.data-experts.net'
alias ishqs='ssh inet@inet-sh-qs-agr.data-experts.net'
alias istqs='ssh inet@inet-st-qs-agr.data-experts.net'
alias isnqs='ssh inet@inet-sn-qs-agr.data-experts.net'

alias ibbpr='ssh inet@inet-bb-pr-vm.nlberlin.deg.local'
alias imvpr='ssh inet@inet-mv-pr-vm.nlberlin.deg.local'
alias ishpr='ssh inet@inet-sh-pr-vm.nlberlin.deg.local'
alias istpr='ssh inet@inet-st-pr-vm.nlberlin.deg.local'
alias ido='ssh inet@010-lc7-inet-docker.nlberlin.deg.local'


ifv() {
	ssh inet@inet-fv-vm.nlberlin.deg.local
}


iscp() {
	# aka "inet-scp"
	scp inet@inet-$2-test.data-experts.net:$1 ~/Downloads
}

# Raspberry Pi
alias ipi='ssh inet@192.168.3.114'

deployforms () {
	mv documentDefinition/* deployment/documentDefinition
}

copform () {
	cp documentDefinition/*.efd.xml ../../../dev/home/documentDefinition
	cp documentDefinition/*.json ../../../dev/home/documentDefinition	
}

# ibash-Befehle
ireport() {
	/opt/iReport-5.6.0/bin/ireport
    sleep 1
	exit
}

tor() {
  cd /opt/tor-browser_en-US/ 1>/dev/null 
  ./start-tor-browser.desktop
  cd - 1>/dev/null 
}

nwRemotePr() {
    remmina -c /home/bzenz/.local/share/remmina/group_rdp_inet-nw-pr-vm-nlberlin-deg-local_inet-nw-pr-vm-nlberlin-deg-local.remmina >/dev/null 2>&1 &
}

laptopScreens() {
    $HOME/.screenlayout/laptop-only.sh
    killall polybar
    $HOME/.config/polybar/launch.sh >/dev/null 2>&1
}

workScreens() {
    $HOME/.screenlayout/laptop-only.sh
    $HOME/.screenlayout/work.sh
    killall polybar
    $HOME/.config/polybar/launch.sh >/dev/null 2>&1
}

homeScreens() {
    $HOME/.screenlayout/laptop-only.sh
    $HOME/.screenlayout/home.sh
    killall polybar
    $HOME/.config/polybar/launch.sh >/dev/null 2>&1
}

###### LOCAL STARTSCRIPTS ########
